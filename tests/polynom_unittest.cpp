#include "gtest/gtest.h"
#include <deque>
#include <sstream>
#include "Polynom.cpp"


TEST(Polynomial, create_polynom_with_int)
{
	std::deque<int> deq;
	deq.push_back(1);
	deq.push_back(2);
	deq.push_back(3);
	deq.push_back(4);
	Polynom<int> pol_1(deq);
	std::string str = "1 + 2x^1 + 3x^2 + 4x^3";
	std::stringstream ss;
	ss << pol_1;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, create_polynom_with_double)
{
	std::deque<double> deq;
	deq.push_back(1.5);
	deq.push_back(2.5);
	deq.push_back(3.4);
	deq.push_back(4.7);
	Polynom<double> pol_1(deq);
	std::string str = "1.5 + 2.5x^1 + 3.4x^2 + 4.7x^3";
	std::stringstream ss;
	ss << pol_1;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, create_polynom_with_braces)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	std::string str = "1 + 2x^1 + 3x^2 + 4x^3";
	std::stringstream ss;
	ss << pol_1;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, create_polynom_with_not_all_coefficients)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[2] = 3;
	pol_1[3] = 4;
	std::string str = "1 + 3x^2 + 4x^3";
	std::stringstream ss;
	ss << pol_1;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, return_coefficient)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	ASSERT_EQ(1, pol_1[0]);
	ASSERT_EQ(2, pol_1[1]);
	ASSERT_EQ(3, pol_1[2]);
	ASSERT_EQ(4, pol_1[3]);
}

TEST(Polynomial, value_in_point)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[2] = 3;
	pol_1[3] = 4;
	int res = pol_1(5);
  ASSERT_EQ(576, res);
}

TEST(Polynomial, is_equal)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 0;
	Polynom<int> pol_2;
	pol_2[0] = 1;
	pol_2[1] = 2;
	pol_2[2] = 3;
	pol_2[3] = 4;
	ASSERT_EQ(pol_1, pol_2);
}

TEST(Polynomial, not_equal)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 0;
	pol_1[5] = 1;
	Polynom<int> pol_2;
	pol_2[0] = 1;
	pol_2[1] = 2;
	pol_2[2] = 3;
	pol_2[3] = 4;
	ASSERT_EQ(1, pol_1 != pol_2);
}

TEST(Polynomial, sum1)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	Polynom<int> pol_2;
	pol_2[0] = 3;
	pol_2[1] = 6;
	pol_2[2] = 7;
	pol_2[3] = 8;
	Polynom<int> pol_3 = pol_1 + pol_2;
	std::string str = "4 + 8x^1 + 10x^2 + 12x^3 + 1x^4";
	std::stringstream ss;
	ss << pol_3;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, sum2)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	Polynom<int> pol_2;
	pol_2[0] = 3;
	pol_2[1] = 6;
	pol_2[2] = 7;
	pol_2[3] = 8;
	pol_1 += pol_2;
	std::string str = "4 + 8x^1 + 10x^2 + 12x^3 + 1x^4";
	std::stringstream ss;
	ss << pol_1;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, sum3)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	pol_1 += 5;
	std::string str = "6 + 2x^1 + 3x^2 + 4x^3 + 1x^4";
	std::stringstream ss;
	ss << pol_1;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, sum4)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	int value = 5;
	Polynom<int> pol_3 = pol_1 + value;
	std::string str = "6 + 2x^1 + 3x^2 + 4x^3 + 1x^4";
	std::stringstream ss;
	ss << pol_3;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, sum5)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	int value = 5;
	Polynom<int> pol_3 = value + pol_1;
	std::string str = "6 + 2x^1 + 3x^2 + 4x^3 + 1x^4";
	std::stringstream ss;
	ss << pol_3;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, difference1)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	Polynom<int> pol_2;
	pol_2[0] = 3;
	pol_2[1] = 6;
	pol_2[2] = 7;
	pol_2[3] = 8;
	Polynom<int> pol_3 = pol_1 - pol_2;
	std::string str = "-2 - 4x^1 - 4x^2 - 4x^3 + 1x^4";
	std::stringstream ss;
	ss << pol_3;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, difference2)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	Polynom<int> pol_2;
	pol_2[0] = 3;
	pol_2[1] = 6;
	pol_2[2] = 7;
	pol_2[3] = 8;
	pol_1 -= pol_2;
	std::string str = "-2 - 4x^1 - 4x^2 - 4x^3 + 1x^4";
	std::stringstream ss;
	ss << pol_1;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, difference3)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	pol_1 -= 5;
	std::string str = "-4 + 2x^1 + 3x^2 + 4x^3 + 1x^4";
	std::stringstream ss;
	ss << pol_1;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, difference4)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	int value = 5;
	Polynom<int> pol_3 = pol_1 - value;
	std::string str = "-4 + 2x^1 + 3x^2 + 4x^3 + 1x^4";
	std::stringstream ss;
	ss << pol_3;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, difference5)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	int value = 5;
	Polynom<int> pol_3 = value - pol_1;
	std::string str = "4 - 2x^1 - 3x^2 - 4x^3 - 1x^4";
	std::stringstream ss;
	ss << pol_3;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, mult1)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	Polynom<int> pol_2;
	pol_2[0] = 3;
	pol_2[1] = 6;
	pol_2[2] = 7;
	pol_2[3] = 8;
	Polynom<int> pol_3 = pol_1 * pol_2;
	std::string str = "3 + 12x^1 + 28x^2 + 52x^3 + 64x^4 + 58x^5 + 39x^6 + 8x^7";
	std::stringstream ss;
	ss << pol_3;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, mult2)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	Polynom<int> pol_2;
	pol_2[0] = 3;
	pol_2[1] = 6;
	pol_2[2] = 7;
	pol_2[3] = 8;
	pol_1 *= pol_2;
	std::string str = "3 + 12x^1 + 28x^2 + 52x^3 + 64x^4 + 58x^5 + 39x^6 + 8x^7";
	std::stringstream ss;
	ss << pol_1;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, mult3)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	pol_1 *= 5;
	std::string str = "5 + 10x^1 + 15x^2 + 20x^3 + 5x^4";
	std::stringstream ss;
	ss << pol_1;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, mult4)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	int value = 5;
	Polynom<int> pol_3 = pol_1 * value;
	std::string str = "5 + 10x^1 + 15x^2 + 20x^3 + 5x^4";
	std::stringstream ss;
	ss << pol_3;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, mult5)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 2;
	pol_1[2] = 3;
	pol_1[3] = 4;
	pol_1[4] = 1;
	int value = 5;
	Polynom<int> pol_3 = value * pol_1;
	std::string str = "5 + 10x^1 + 15x^2 + 20x^3 + 5x^4";
	std::stringstream ss;
	ss << pol_3;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, div1)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 3;
	pol_1[2] = 3;
	pol_1[3] = 1;
	Polynom<int> pol_2;
	pol_2[0] = 1;
	pol_2[1] = 2;
	pol_2[2] = 1;
	Polynom<int> pol_3 = pol_1 / pol_2;
	std::string str = "1 + 1x^1";
	std::stringstream ss;
	ss << pol_3;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, div2)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 3;
	pol_1[2] = 3;
	pol_1[3] = 1;
	Polynom<int> pol_2;
	pol_2[0] = 1;
	pol_2[1] = 2;
	pol_2[2] = 1;
	pol_1 /= pol_2;
	std::string str = "1 + 1x^1";
	std::stringstream ss;
	ss << pol_1;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, div3)
{
	Polynom<int> pol_1;
	pol_1[0] = 3;
	pol_1[1] = 6;
	pol_1[2] = 9;
	pol_1[3] = 9;
	pol_1[4] = 12;
	pol_1 /= 3;
	std::string str = "1 + 2x^1 + 3x^2 + 3x^3 + 4x^4";
	std::stringstream ss;
	ss << pol_1;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, div4)
{
	Polynom<int> pol_1;
	pol_1[0] = 3;
	pol_1[1] = 6;
	pol_1[2] = 9;
	pol_1[3] = 9;
	pol_1[4] = 12;
	int value = 3;
	Polynom<int> pol_3 = pol_1 / value;
	std::string str = "1 + 2x^1 + 3x^2 + 3x^3 + 4x^4";
	std::stringstream ss;
	ss << pol_3;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, div5)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 3;
	pol_1[2] = 3;
	pol_1[3] = 4;
	Polynom<int> pol_2;
	pol_2[0] = 1;
	pol_2[1] = 2;
	pol_2[2] = 1;
	Polynom<int> pol_3 = pol_1 / pol_2;
	std::string str = "-5 + 4x^1";
	std::stringstream ss;
	ss << pol_3;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, modulo1)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 3;
	pol_1[2] = 3;
	pol_1[3] = 1;
	Polynom<int> pol_2;
	pol_2[0] = 1;
	pol_2[1] = 2;
	pol_2[2] = 1;
	Polynom<int> pol_3 = pol_1 % pol_2;
	std::string str = "zero polynom";
	std::stringstream ss;
	ss << pol_3;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, modulo2)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 3;
	pol_1[2] = 3;
	pol_1[3] = 1;
	Polynom<int> pol_2;
	pol_2[0] = 1;
	pol_2[1] = 2;
	pol_2[2] = 1;
	pol_1 %= pol_2;
	std::string str = "zero polynom";
	std::stringstream ss;
	ss << pol_1;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, modulo3)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 3;
	pol_1[2] = 3;
	pol_1[3] = 4;
	Polynom<int> pol_2;
	pol_2[0] = 1;
	pol_2[1] = 2;
	pol_2[2] = 1;
	Polynom<int> pol_3 = pol_1 % pol_2;
	std::string str = "6 + 9x^1";
	std::stringstream ss;
	ss << pol_3;
  ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, gcd)
{
	Polynom<int> pol_1;
	pol_1[0] = 1;
	pol_1[1] = 3;
	pol_1[2] = 3;
	pol_1[3] = 1;
	Polynom<int> pol_2;
	pol_2[0] = 1;
	pol_2[1] = 2;
	pol_2[2] = 1;
	Polynom<int> pol_3 = (pol_1, pol_2);
	Polynom<int> pol_4 = (pol_2, pol_1);
	std::string str = "1 + 2x^1 + 1x^2";
	std::stringstream ss, ss1;
	ss << pol_3;
	ss1 << pol_4;
  ASSERT_EQ(ss.str(), str);
	ASSERT_EQ(ss1.str(), str);
}

TEST(Polynomial, example1)
{
	Polynom<double> a(3);
	a[5] = 42.0;
	ASSERT_EQ(42, a[5]);
}

TEST(Polynomial, example2)
{
	Polynom<int> a;
	const Polynom<int>& b = a;
	a[5] = 42.0;
	ASSERT_EQ(42, b[5]);
}

TEST(Polynomial, example3)
{
	Polynom<int> a;
	a[1] = 1;
	a[3] = 3;
	a[5] = 5;
	a[5] = 0;
	ASSERT_EQ(3, a.degree());
}

TEST(Polynomial, example4)
{
	Polynom<int> a;
	a[0] = 3;
	a[1] = 5;
	const Polynom<int> b = a;
	std::stringstream ss;
	std::string str = "3 5 ";
	for (auto it = b.begin(); it != b.end(); ++it)
    	ss << *it << " ";
	ASSERT_EQ(ss.str(), str);
}

TEST(Polynomial, example5)
{
	Polynom<double> a(3);
	const auto& b = a;
	const double& c5 = b[5];
	a[5] = 42.0;
	ASSERT_EQ(c5, 42.0);
}
