This project is an implementation of polynom.

use your console:

# make build directory
mkdir build
cd build

# generate projects
cmake ..

# build exe and unittests
cmake --build .
```
To start work with the polynom include Polynom.cpp in your programm.

You can use following operations:
1. +
2. +=
3. -
4. -=
5. *
6. *=
7. /
8. /=
9. %
10. %= 
11. ,
12. begin()
13. end()

