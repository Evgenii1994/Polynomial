#include <deque>
#include <iostream>
#include <cmath>

template <typename T>
class Polynom {

  std::deque<T> weights;

public:

  Polynom() {}
  Polynom(T weight) {
    weights.push_back(weight);
  }
  Polynom(std::deque<T>& _weights) {
    weights = _weights;
  }

  void update() {
    while (!this->weights.empty() && this->weights.back() == 0)
        this->weights.pop_back();
  }

  int degree() const {
    int degree = -1;
    for (int i = 0; i < this->weights.size(); ++i) {
      if (this->weights[i] != 0) {
        if (degree < i) {
          degree = i;
        }
      }
    }
    return degree;
  }


  friend std::ostream& operator<< (std::ostream& os, const Polynom& _polynom) {
    if (_polynom.weights.empty()) {
      os << "zero polynom";
      return os;
    }
    int flag = 0;
    for (int i = 0; i <_polynom.weights.size(); ++i) {
      if (_polynom.weights[i] == 0) {
        continue;
      }
      if (_polynom.weights[i] != 0 && flag == 0){
        flag = 1;
        if (i != 0) {
          os << _polynom.weights[i] << "x^" << i;
        } else {
          os << _polynom.weights[i];
        }
        continue;
      }
      if (_polynom.weights[i] != 0) {
        if (_polynom.weights[i] < 0) {
          os << " - ";
        } else {
          os << " + ";
        }
        os << std::abs(_polynom.weights[i]) << "x^" << i;
      }
    }
    return os;
  }


  T& operator[] (int index) {
    if (this->weights.size() <= index) {
      this->weights.resize(index + 1);
    }
    return this->weights[index];
  }
  const T& operator[] (int index) const {
    return this->weights[index];
  }


  const T operator() (T value) const {
    T res = 0;
    for (int i = 0; i < this->weights.size(); ++i) {
      res += pow(value, i) * this->weights[i];
    }
    return res;
  }


  bool operator== (const Polynom& right) const {
    if (this->degree() == right.degree()) {
      for (int i = 0; i < this->degree() + 1; ++i) {
        if (this->weights[i] != right.weights[i]) {
          return false;
        }
      }
    } else {
      return false;
    }
    return true;
  }


  bool operator!= (const Polynom& right) const {
    return !(*this == right);
  }


  friend Polynom operator+ (T left, Polynom& right) {
    Polynom result = right;
    result.weights[0] += left;
    return result;
  }
  Polynom operator+ (T right) const {
    Polynom result = *this;
    result.weights[0] += right;
    return result;
  }


  friend Polynom operator- (T left, Polynom& right) {
    Polynom result = right;
    result.weights[0] -= left;
    for (auto& i : result) {
      i *= -1;
    }
    return result;
  }
  Polynom operator- (T right) const{
    Polynom result = *this;
    result.weights[0] -= right;
    return result;
  }


  friend Polynom operator* (T left, Polynom& right) {
    Polynom result = right;
    for (int i = 0; i < result.weights.size(); ++i) {
      result.weights[i] *= left;
    }
    return result;
  }
  Polynom operator* (T right) const {
    Polynom result = *this;
    for (int i = 0; i < result.weights.size(); ++i) {
      result.weights[i] *= right;
    }
    return result;
  }


  const Polynom operator+= ( T right) {
    *this = *this + right;
        return *this;
  }


  const Polynom operator-= (T right) {
    *this = *this - right;
        return *this;
  }


  const Polynom operator*= (T right) {
    *this = *this * right;
        return *this;
  }


  Polynom operator+ (const Polynom& right) const {
    Polynom result = *this;
    if (result.weights.size() < right.weights.size()) {
      result.weights.resize(right.weights.size());
    }
    for (int i = 0; i < right.weights.size(); ++i) {
      result.weights[i] += right.weights[i];
    }
    result.update();
    return result;
  }


  Polynom operator- (const Polynom& right) const {
    Polynom result = *this;
    if (result.weights.size() < right.weights.size()) {
      result.weights.resize(right.weights.size());
    }
    for (int i = 0; i < right.weights.size(); ++i) {
      result.weights[i] -= right.weights[i];
    }
    result.update();
    return result;
  }


  Polynom operator* (const Polynom& right) const {
    Polynom<T> result;
    result.weights.resize(this->degree() + right.degree() + 1);
    for (int i = 0; i < this->weights.size(); ++i) {
        for (int j = 0; j < right.weights.size(); ++j) {
            result[i + j] += this->weights[i] * right.weights[j];
        }
    }
    result.update();
    return result;
  }


  const Polynom operator*= (const Polynom& right) {
    *this = *this * right;
        return *this;
  }


  const Polynom operator+= (const Polynom& right) {
    *this = *this + right;
    return *this;
  }


  const Polynom operator-= (const Polynom& right) {
    *this = *this - right;
    return *this;
  }


  Polynom operator/ (const Polynom& right) const {
    Polynom ostatok = *this;
    Polynom chastnoe;
    while (right.degree() <= ostatok.degree()) {
      Polynom part;
      part[ostatok.degree() - right.degree()] = ostatok.weights.back() / right.weights.back();
      ostatok -= right * part;
      chastnoe += part;
    }
    chastnoe.update();
    return chastnoe;
  }
  const Polynom operator/= (const Polynom& right) {
    *this = *this / right;
    return *this;
  }
  Polynom operator/ (const T right) const {
    Polynom result = *this;
    for (int i = 0; i < result.weights.size(); ++i) {
      result.weights[i] /= right;
    }
    return result;
  }

  Polynom operator% (const Polynom& right) const {
    Polynom ostatok = *this;
    while (right.degree() <= ostatok.degree()) {
      Polynom part;
      part[ostatok.degree() - right.degree()] = ostatok.weights.back() / right.weights.back();
      ostatok -= right * part;
    }
    return ostatok;
  }
  const Polynom operator%= (const Polynom& right) {
    *this = *this % right;
    return *this;
  }


  friend Polynom operator, (Polynom& left, Polynom& right) {
    Polynom temp;
    while (right.degree() != -1) {
      temp = right;
      right = left % right;
      left = temp;
    }
    return left;
  }


  typename std::deque<T>::iterator begin()
  {
      return weights.begin();
  }

  typename std::deque<T>::iterator end()
  {
      return weights.end();
  }

  typename std::deque<T>::const_iterator begin() const
  {
      return weights.begin();
  }

  typename std::deque<T>::const_iterator end() const
  {
      return weights.end();
  }
};
